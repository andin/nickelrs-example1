#[macro_use] extern crate nickel;

use nickel::{Nickel, HttpRouter, Middleware, Request, Response, MiddlewareResult};
use std::sync::Mutex;
use std::sync::Arc;

struct Handler<D, F> { 
    data: Arc<Mutex<D>>, func: F 
}

impl<D, F> Middleware<D> for Handler<D, F> where F: Fn(Request, Response) + Send + Sync + 'static, D: Send + Sync + 'static {
    fn invoke<'mw, 'conn>(&self, req: &mut Request<'mw, 'conn, D>, res: Response<'mw, D>) -> MiddlewareResult<'mw, D> { 
        let lock = self.data.lock(); 
        (self.func)(req, res) 
    }
}

pub struct Config {
   pub data: String
}

impl Config {
    pub fn handler<'mw>(&mut self, req: &mut Request, res: Response<'mw>) -> MiddlewareResult<'mw> {
        res.send(self.data.as_str())
    }
}

fn main() {
    let mut config = Config {
        data: "hello".to_string(),
    };

    let mut server = Nickel::new();
    // WORKS: server.get("**", middleware!("Hello World"));
    server.get("**", config.handler);

    server.listen("127.0.0.1:6767");
}